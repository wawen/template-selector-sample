import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {GuestBooksComponent } from './display-books.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DisplayBooksComponent', () => {
  let component:GuestBooksComponent;
  let fixture: ComponentFixture<DisplayBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GuestBooksComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
