import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from 'src/app/shared/models/book';
import { BookService } from 'src/app/shared/services/book.service';

@Component({
  selector: 'app-guest-books',
  templateUrl: './guest-books.component.html',
  styleUrls: ['./guest-books.component.scss']
})
export class GuestBooksComponent implements OnInit {
  public books$: Observable<Book[]>;
  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books$ = this.bookService.getBooks();
  }
}
