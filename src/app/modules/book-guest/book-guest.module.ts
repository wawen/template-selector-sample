import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestBooksComponent } from './components/guest-books/guest-books.component';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';



@NgModule({
  declarations: [GuestBooksComponent],
  imports: [
    CommonModule,
    MatCardModule,
    RouterModule.forChild([{path: '', component: GuestBooksComponent}])
  ]
})
export class BookGuestModule { }
