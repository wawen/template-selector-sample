import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ROUTES, Routes } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
    {
      provide: ROUTES,
      useFactory: configBookHandlerRoutes,
      deps: [AuthService],
      multi: true
    }
  ]
})


export class BookHandlerModule {}

export function configBookHandlerRoutes(authService: AuthService) {
  let routes: Routes = [];
  if (authService.isAuthorized() == 't1') {
    routes = [
      {
        path: '', loadChildren: () => import('../book-manager/book-manager.module').then(mod => mod.BookManagerModule)
      }
    ];
  } else if (authService.isAuthorized() == 't2') {
    routes = [
      {
        path: '', loadChildren: () => import('../book-consumer/book-consumer.module').then(mod => mod.BookConsumerModule)
      }
    ];
  } else {
    routes = [
      {
        path: '', loadChildren: () => import('../book-guest/book-guest.module').then(mod => mod.BookGuestModule)
      }
    ];
  }
  return routes;
}
