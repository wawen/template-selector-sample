import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public authorized$: Observable<any>;
  private authorizedSource: BehaviorSubject<any>;
  constructor(private router: Router) {
    this.authorizedSource = new BehaviorSubject<any>(false);
    this.authorized$ = this.authorizedSource.asObservable();
  }

  public isAuthorized(): any {
    return this.authorizedSource.value;
  }

  public setAuthorized(value: any): void {
    const previous = this.authorizedSource.value;
    this.authorizedSource.next(value);
    if (previous === this.authorizedSource.value) {
      return;
    }
    const i = this.router.config.findIndex(x => x.path === 'books');
    this.router.config.splice(i, 1);
    this.router.config.push(
      {path: 'books', loadChildren: () => import('../../modules/book-handler/book-handler.module').then(mod => mod.BookHandlerModule)}
    );
  }
}
